console.log("Hello World");

// Addition function
function printSum(sumNum1,sumNum2){
	console.log("Displayed sum of " + sumNum1 + " and " + sumNum2);

	let sumTotal = sumNum1 + sumNum2;
	console.log(sumTotal);
};

printSum(5,15);

// Subtraction function
function printDifference(diffNum1,diffNum2){
	console.log("Displayed difference of " + diffNum1 + " and " + diffNum2);

	let diffTotal = diffNum1 - diffNum2;
	console.log(diffTotal);
};

printDifference(20,5);

// Multiplication function
function printProduct(productNum1,productNum2){
	console.log("The product of " + productNum1 + " and " + productNum2 + ":");

	return productNum1 * productNum2;
};

let product = printProduct(50,10);
console.log(product)

// Division function
function printQuotient(quotientNum1,quotientNum2){
	console.log("The quotient of " + quotientNum1 + " and " + quotientNum2 + ":");

	return quotientNum1 / quotientNum2;
};

let quotient = printQuotient(50,10);
console.log(quotient);

// Area of Circle function
function printCircleArea(radius){
	console.log("The result of getting the area of a circle with " + radius + " radius:");

	// Math.PI - pi value, " ** " - exponent function
	return Math.PI * radius ** 2;
};

let circleArea = printCircleArea(15);
console.log(circleArea);

// Average function
function printAverage(averageNum1,averageNum2,averageNum3,averageNum4){
	console.log("The average of " + averageNum1 + ", " + averageNum2 + ", " + averageNum3 + " and " + averageNum4 + ":");

	return (averageNum1+averageNum2+averageNum3+averageNum4) / 4;
};

let averageVar = printAverage(20,40,60,80);
console.log(averageVar);

// Check function
function printCheck(checkNum1,checkNum2){
	console.log("Is " + checkNum1 + "/" + checkNum2 + " a passing score?");

	let percentage = (checkNum1/100) * checkNum2;
	let isPassed = percentage <= 75;

	return isPassed;
};

let isPassingScore = printCheck(38,50);
console.log(isPassingScore);