// Function able to receive data without the use of global variables or prompt();

// "name" - is a parameter
// A paramenter is a variable/container that exists only in our function and is used to store information that is provided to a function when it is called/invoked.
function printName(name){
	console.log("My name is " + name);
};

// Data passed into a function invocation can be received by the function
// This is what we call an argument
printName("Juana");
printName("Jimin");

// Data passed into the function through function invocation is called arguments
// The arguments is then within a container called a parameter

function printMyAge(age){
	console.log("I am " + age);
};

printMyAge(25);
printMyAge();

// check divisibilit reusably using a function with arguments and parameters
function checkDivisibilityBy8(num){
	let remainder = num % 8;
	console.log("The remainder of " + num + " divided by 8 is: " + remainder);
	let isDivisibleby8 = remainder === 0;
	console.log("Is " + num + " divisible by 8?");
	console.log(isDivisibleby8);

};

checkDivisibilityBy8(64);
checkDivisibilityBy8(27);

function userFavoriteHero(heroname){
	console.log("My favorite super hero is " + heroname);
};

userFavoriteHero("Ironman");

function printMyFavoriteLanguage(language){
	console.log("My favorite language is: " + language)
};

printMyFavoriteLanguage("Javascript");
printMyFavoriteLanguage("Java");

// Multiple Arguments can also be passed into a function;

function printFullName(firstName,middleName,lastName){
	console.log(firstName + " " + middleName + " " + lastName);
};

printFullName("Juan","Crisostomo","Ibarra");
printFullName("Ibarra","Juan","Crisostomo");
/*
	Parameters will contain the arguments according to the order it was passed.

	"Juan" - firstName
	"Crisostomo" - middleName
	"Ibarra" - lastName

	In other languages, providing more/less arguments than the expected parameters sometimes causes an error or changes the behavior of the function.

	In Javascript, we don't have to worry about that.
*/

printFullName("Stephen","Wardell");
printFullName("Stephen","Wardell","Curry");
printFullName("Stephen","Wardell","Curry","James");
// commas are used to separate values.
// printFullName("Stephen""Wardell","Curry");

// Use Variables as Arguments
let fName = "Larry";
let mName = "Joe";
let lName = "Bird";

printFullName(fName,mName,lName);

function printFavoriteSongs(song1,song2,song3,song4,song5){
	console.log("My Favorite Songs: ");
	console.log("1. " + song1);
	console.log("2. " + song2);
	console.log("3. " + song3);
	console.log("4. " + song4);
	console.log("5. " + song5);
}

printFavoriteSongs("Deja Vu","Mr. Brightside","Bulong","Muli","Do You Believe in Me");

// Return Statement

// Currently or so far, our functions are able to display data in our console.
// However, our functions cannot yet return values. Functions are able to return values which can be saved into a variable using return statment/keyword

let fullName = printFullName("Mark","Joseph","Lacdao");
console.log(fullName);

function returnFullName(firstName,middleName,lastName){

	return firstName + " " + middleName + " " + lastName;
};

fullName = returnFullName("Ernesto","Antonio","Maceda");
console.log(fullName);

// Functions which have a return statement are able to return value and it can be saved in a variable.

console.log(fullName + " is my grandpa.");

function returnPhilippineAddress(city){

	return city + ", Philippines"
};

let myFullAddress = returnPhilippineAddress("Manila");
console.log(myFullAddress);

// returns true if number is divisible by 4, else, returns false.
function checkDivisibilityBy4(num){

	let remainder = num % 4;
	let isDivisibleby4 = remainder === 0;

	// returns either true or false
	// Not only can you return raw values/data, you can also directly return a variable
	return isDivisibleby4;
	// Return keyword not only allows us to return value but also ends the process of our function.
	console.log("I am run after the return");
};

let num4isDivisibleBy4 = checkDivisibilityBy4(4);
let num14isDivisibleBy4 = checkDivisibilityBy4(14);

console.log(num4isDivisibleBy4);
console.log(num14isDivisibleBy4);

function createPlayerInfo(username,level,job){

	// console.log("username: " + username + ", level: " + level + ", job: " + job); // result: works but there is an undefined below.

	return "username: " + username + ", level: " + level + ", job: " + job;

};

let user1 = createPlayerInfo("white_night",95,"Paladin");
console.log(user1);